package edu.java.customdependency;

import edu.endava.collection.CustomHashMap;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class Application {
    private static final String addStudentLogger = "Add new student to StudentHashMap, previous key value = ";

    public static void main(String[] args) {
        CustomHashMap<Student, Integer> studentMap = new CustomHashMap<>();

        Student artur = new Student("Artur", LocalDate.of(1999, 9, 15), "A good guy");
        Student victor = new Student("Victor", LocalDate.of(1982, 9, 11), "A good guy2");
        Student sergiu = new Student("Sergiu", LocalDate.of(1999, 9, 14), "A good guy3");
        Student maria = new Student("Maria", LocalDate.of(2000, 6, 21), "A smart girl");
        Student petru = new Student("Petru", LocalDate.of(1986, 10, 19), "A tall man");

        // put()
        log.info(addStudentLogger + studentMap.put(artur, 100));
        log.info(addStudentLogger + studentMap.put(maria, 2));
        log.info(addStudentLogger + studentMap.put(victor, 8));
        log.info(addStudentLogger + studentMap.put(sergiu, 9));
        log.info(addStudentLogger + studentMap.put(petru, 4));

        // toString()
        log.info("The new array is: " + studentMap);

        // size()
        log.info("Map size: " + studentMap.size());

        // isEmpty()
        log.info("Map is empty: " + studentMap.isEmpty());

        // get()
        log.info("artur value: " + studentMap.get(artur));

        // containsKey()
        log.info("Map contains key artur: " + studentMap.containsKey(artur));

        // containsValue()
        log.info("Map contains value 55: " + studentMap.containsValue(55));

        // putAll()
        Map<Student, Integer> studentMapTest = new HashMap<>();
        studentMapTest.put(new Student("Alex", LocalDate.of(1995, 7, 30), "Test guy"), 12);
        studentMapTest.put(new Student("Pavel", LocalDate.of(1990, 3, 20), "Test guy 2"), 18);
        studentMap.putAll(studentMapTest);
        log.info("Map after putAll: " + studentMap);

        // values()
        log.info("Map values: " + studentMap.values());

        // keySet()
        log.info("Map keys: " + studentMap.keySet());

        // remove()
        log.info("Last victor value: " + studentMap.remove(victor));
        log.info("Map after removing victor: " + studentMap);

        // entrySet()
        log.info("Students entryset: " + studentMap.entrySet());

        // clear
        studentMap.clear();
        log.info("Map after clear: " + studentMap);
    }
}
