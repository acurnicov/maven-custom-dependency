package edu.java.customdependency;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class StudentTest {
    private Student artur = new Student("Artur", LocalDate.of(1999, 9, 15), "A good boy");
    private Student artur2 = new Student("Artur", LocalDate.of(1999, 9, 15), null);
    private Student sergiu = new Student("Sergiu", LocalDate.of(1986, 6, 3), "A smart boy");

    private LocalDate arturDate = LocalDate.of(1999, 9, 15);
    private String arturName = "Artur";

    @Test
    public void testToString() {
        assertThat(artur.toString()).contains(arturName, arturDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
    }

    @Test
    public void testGetName() {
        assertThat(artur.getName()).isEqualTo(arturName);
    }

    @Test
    public void testGetDateOfBirth() {
        assertThat(artur.getDateOfBirth().getYear()).isEqualTo(arturDate.getYear());
    }

    @Test
    public void getDetails() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(artur.getDetails()).isNotNull();
            softly.assertThat(artur2.getDetails()).isNotNull();
        });
    }

    @Test
    public void testEquals() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(artur).isEqualTo(artur2);
            softly.assertThat(artur).isNotEqualTo(sergiu);
        });
    }

    @Test
    public void testHashCode() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(artur.hashCode()).isEqualTo(artur2.hashCode());
            softly.assertThat(artur.hashCode()).isNotEqualTo(sergiu.hashCode());
        });
    }
}